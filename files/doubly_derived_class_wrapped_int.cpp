struct Int { int v; };
struct DInt : Int { };
struct EInt : DInt { };
constexpr int f(int n) {
  EInt i = {0};
  for (EInt k = {0}; k.v<10000; ++k.v) {
    i.v += k.v;
  }
  return n;
}
template<int N> struct S {
  static constexpr int sm = S<N-1>::sm+f(N);
};
template<> struct S<0> {
  static constexpr int sm = 0;
};
constexpr int r = S<200>::sm;

