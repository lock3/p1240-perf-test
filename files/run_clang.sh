#!/bin/sh
echo "Running Clang Tests"
echo "Raw Int Test"
time clang++ -std=c++17 -c raw_int.cpp
echo "Wrapped Int Test"
time clang++ -std=c++17 -c wrapped_int.cpp
echo "Derived Wrapped Int Test"
time clang++ -std=c++17 -c derived_class_wrapped_int.cpp
echo "Doubly Derived Wrapped Int Test"
time clang++ -std=c++17 -c doubly_derived_class_wrapped_int.cpp
echo "Access Through Member Function Wrapped Int Test"
time clang++ -std=c++17 -c access_through_member_fn_wrapped_int.cpp
echo "Value Class Wrapped Int Test"
time clang++ -std=c++17 -c value_class_wrapped_int.cpp
