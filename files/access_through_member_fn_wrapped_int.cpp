struct Int {
  constexpr Int(int v) : v(v) { }
  constexpr operator int&() { return this->v; }
private:
  int v;
};
constexpr int f(int n) {
  Int i = {0};
  for (Int k = {0}; k<10000; ++k) {
    i += k;
  }
  return n;
}
template<int N> struct S {
  static constexpr int sm = S<N-1>::sm+f(N);
};
template<> struct S<0> {
  static constexpr int sm = 0;
};
constexpr int r = S<200>::sm;

