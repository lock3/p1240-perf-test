struct Int { int v; };
constexpr int f(int n) {
  Int i = {0};
  for (Int k = {0}; k.v<10000; ++k.v) {
    i.v += k.v;
  }
  return n;
}
template<int N> struct S {
  static constexpr int sm = S<N-1>::sm+f(N);
};
template<> struct S<0> {
  static constexpr int sm = 0;
};
constexpr int r = S<200>::sm;

