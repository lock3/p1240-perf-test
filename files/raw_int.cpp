constexpr int f(int n) {
  int i = 0;
  for (int k = 0; k<10000; ++k) {
    i += k;
  }
  return n;
}

template<int N> struct S {
  static constexpr int sm = S<N-1>::sm+f(N);
};
template<> struct S<0> {
  static constexpr int sm = 0;
};
constexpr int r = S<200>::sm;

