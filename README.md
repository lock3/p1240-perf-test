## p1240 Performance Test

To run the tests on your own machine, you must have docker setup.
Additionally it is assumed you're logged in as a user in the docker
group.

Then you can simply:
```
cd build_sys/
./enter_build_env.sh
```

This will put you inside of a docker build environment with the latest versions
of gcc and clang installed per arch linux packaging.

Once inside the build environment, you can run clang tests via:
```
./run_clang.sh
```

You should see output similar to the following:
```
build env:/build_env$ ./run_clang.sh
Running Clang Tests
Raw Int Test

real    0m2.642s
user    0m2.621s
sys     0m0.020s
Wrapped Int Test

real    0m3.332s
user    0m3.309s
sys     0m0.020s
Derived Wrapped Int Test

real    0m4.233s
user    0m4.215s
sys     0m0.017s
Doubly Derived Wrapped Int Test

real    0m4.802s
user    0m4.722s
sys     0m0.036s
Access Through Member Function Wrapped Int Test

real    0m7.339s
user    0m7.308s
sys     0m0.029s
Value Class Wrapped Int Test

real    0m14.429s
user    0m14.391s
sys     0m0.034s
```

Similarly to clang, gcc tests can be run via:
```
./run_gcc.sh
```


You should see output similar to the following:
```
build env:/build_env$ ./run_gcc.sh
Running GCC Tests
Raw Int Test

real    0m2.606s
user    0m2.549s
sys     0m0.056s
Wrapped Int Test

real    0m3.202s
user    0m3.153s
sys     0m0.048s
Derived Wrapped Int Test

real    0m3.731s
user    0m3.671s
sys     0m0.059s
Doubly Derived Wrapped Int Test

real    0m4.123s
user    0m4.051s
sys     0m0.071s
Access Through Member Function Wrapped Int Test

real    0m15.999s
user    0m15.540s
sys     0m0.447s
Value Class Wrapped Int Test

real    0m15.954s
user    0m15.208s
sys     0m0.740s
```
